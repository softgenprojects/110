import { format } from 'date-fns';

export const FormatDate = (date, formatType = 'date') => {
  if (formatType === 'date') {
    return format(date, 'MMMM dd, yyyy');
  } else if (formatType === 'time') {
    return format(date, 'HH:mm:ss');
  }
};