import { Box, VStack, Heading, Button, Text, useColorModeValue, Center, Image, SimpleGrid, Link, AspectRatio, useBreakpointValue, Grid, GridItem, Flex } from '@chakra-ui/react';
import { BrandLogo } from '@components/BrandLogo';
import { ShopDetailsComponent } from '@components/ShopDetailsComponent';

const Home = () => {
  const bgColor = useColorModeValue('gray.50', 'gray.800');
  const textColor = useColorModeValue('gray.800', 'whiteAlpha.900');
  const buttonSize = useBreakpointValue({ base: 'md', md: 'lg' });

  return (
    <Box bg={bgColor} color={textColor} minH="100vh" py="12" px={{ base: '4', lg: '8' }}>
      <VStack spacing="8">
        <BrandLogo />
        <Heading as="h1" size="2xl" textAlign="center">
          Welcome to LA Vet Shop
        </Heading>
        <Text fontSize={{ base: 'md', md: 'lg', lg: 'xl' }} textAlign="center">
          Your one-stop destination for all your pet's needs in Los Angeles.
        </Text>
        <AspectRatio ratio={16 / 9} width="full">
          <iframe
            title="LA Vet Shop Intro"
            src="https://www.youtube.com/embed/dQw4w9WgXcQ"
            allowFullScreen
          />
        </AspectRatio>
        <Grid templateColumns={{ base: 'repeat(1, 1fr)', md: 'repeat(2, 1fr)' }} gap={6} width="full">
          <GridItem>
            <Flex direction="column" align="center" textAlign="center">
              <Image src="https://i.pravatar.cc/300" borderRadius="full" boxSize="150px" mx="auto" />
              <Text mt="4" fontSize="xl" fontWeight="bold">
                Veterinary Services
              </Text>
              <Text mt="2">
                Comprehensive care and emergency services for your pets.
              </Text>
              <Button mt="4" colorScheme="teal" size={buttonSize}>
                Learn More
              </Button>
            </Flex>
          </GridItem>
          <GridItem>
            <Flex direction="column" align="center" textAlign="center">
              <Image src="https://i.pravatar.cc/300" borderRadius="full" boxSize="150px" mx="auto" />
              <Text mt="4" fontSize="xl" fontWeight="bold">
                Pet Supplies
              </Text>
              <Text mt="2">
                Find all your pet's needs, from food to toys, all in one place.
              </Text>
              <Link href="/shop" style={{ textDecoration: 'none' }}>
                <Button mt="4" colorScheme="teal" size={buttonSize}>
                  Shop Now
                </Button>
              </Link>
            </Flex>
          </GridItem>
        </Grid>
        <ShopDetailsComponent />
      </VStack>
    </Box>
  );
};

export default Home;