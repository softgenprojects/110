import { useQuery } from 'react-query';

export const useShopDetails = () => {
  const shopDetailsData = {
    id: '1',
    name: 'LA Vet Shop',
    description: 'High-quality pet products and accessories.',
    address: '123 Pet Street, Los Angeles, CA',
    hours: '9am - 8pm',
    contact: 'contact@lavetshop.com'
  };

  const query = useQuery('shopDetails', () => Promise.resolve(shopDetailsData));
  return query;
};