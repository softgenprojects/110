import React from 'react';
import { Box, Text, Heading, Spinner, Alert, AlertIcon, AlertTitle, AlertDescription, Center, Button, useColorModeValue, VStack } from '@chakra-ui/react';
import { useShopDetails } from '@hooks/useShopDetails.js';

export const ShopDetailsComponent = () => {
  const { data: shopDetails, isLoading, error } = useShopDetails();
  const bgColor = useColorModeValue('white', 'gray.800');

  if (isLoading) {
    return (
      <Center h='100vh'>
        <Spinner size='xl' />
      </Center>
    );
  }

  if (error) {
    return (
      <Alert status='error'>
        <AlertIcon />
        <AlertTitle mr={2}>Failed to load!</AlertTitle>
        <AlertDescription>There was an issue loading the shop details.</AlertDescription>
      </Alert>
    );
  }

  return (
    <Box
      p={5}
      shadow='xl'
      borderWidth='1px'
      borderRadius='lg'
      bg={bgColor}
      maxW='3xl'
      mx='auto'
      my={10}
    >
      <VStack spacing={5} align='start'>
        <Heading fontSize='3xl'>{shopDetails?.name}</Heading>
        <Text fontSize='lg'>{shopDetails?.description}</Text>
        <Text>Location: {shopDetails?.address}</Text>
        <Button colorScheme='blue' onClick={() => console.log('Learn More Clicked')}>Learn More</Button>
      </VStack>
    </Box>
  );
};