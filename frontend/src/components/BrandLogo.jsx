import { Text, HStack, Image } from '@chakra-ui/react';

export const BrandLogo = () => (
  <HStack spacing={[2, 4]} alignItems="center">
    <Image
      src="https://placehold.co/100x100?text=LA+Vet+Shop"
      alt="LA Vet Shop Logo"
      boxSize={{ base: '50px', md: '60px', lg: '70px' }}
      objectFit="cover"
    />
    <Text
      fontSize={{ base: 'xl', md: '2xl', lg: '3xl' }}
      fontWeight="bold"
      color="teal.500"
    >
      LA Vet Shop
    </Text>
  </HStack>
);