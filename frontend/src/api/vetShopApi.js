import axios from 'axios';

const vetShopApi = axios.create({
  baseURL: 'https://api.vetshopla.com',
  timeout: 1000,
});

export const getShopDetails = async () => {
  try {
    const response = await vetShopApi.get('/shop/details');
    return response;
  } catch (error) {
    console.error('Error fetching shop details:', error);
    throw error;
  }
};

export const getServices = async () => {
  try {
    const response = await vetShopApi.get('/services');
    return response;
  } catch (error) {
    console.error('Error fetching services:', error);
    throw error;
  }
};

export const bookAppointment = async (appointmentDetails) => {
  try {
    const response = await vetShopApi.post('/appointments/book', appointmentDetails);
    return response;
  } catch (error) {
    console.error('Error booking appointment:', error);
    throw error;
  }
};